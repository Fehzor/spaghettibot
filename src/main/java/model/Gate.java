/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import static bot.SuperRandom.oRan;
import bot.command.CommandParser;
import bot.main.IO;
import data.InventoryField;
import data.NumberField;
import data.UserData;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.util.Snowflake;
import model.chatweapons.ChatWeapon;
import org.apache.commons.lang.WordUtils;
import threads.GateEvent;

/**
 *
 * @author FF6EB
 */
public class Gate {
    public long ID;
    public IO io;
    public NumberField messages;
    
    public InventoryField minerals;
    
    public GateEvent gevent;
    
    public Gate(IO io, long ID){
        this.ID = ID;
        this.io = io;
        io.client.getEventDispatcher()
                .on(MessageCreateEvent.class)
                .map(MessageCreateEvent::getMessage)
                .subscribe(mess -> listen(mess));
        
        messages = new NumberField(0l, "gate","messages",""+ID);
        
        minerals = new InventoryField("gate","minerals",""+ID);
        
        gevent = new GateEvent (io, this);
    }
    
    private void listen(Message mess){
        if(mess.getAuthor().get().isBot()){
            //If it's a bot, we don't listen to what it tells us.
            return;
        }
        try{
            if(mess.getChannelId().asLong() == this.ID){
                this.giveMin(mess);
                if(mess.getContent().get().toLowerCase().equals(">gate")){
                    io.send(this+"", Snowflake.of(this.ID));
                }
                
                if(mess.getContent().get().toLowerCase().contains(">deposit")&&
                        mess.getContent().get().charAt(0)=='>'){
                    this.deposit(mess);
                }
                UserData UD = UserData.getUD(mess.getAuthor().get());
                gevent.ping(UD);
                activateWeapon(UD);
            }
        } catch (Exception E){
            E.printStackTrace();
            System.err.println("BAD MESSAGE (Probably a picture rofl)");
        }
    }
    
    private void deposit(Message mess){
        UserData UD = UserData.getUD(mess.getAuthor().get());
        String [] split = mess.getContent().get().split(" ");
        if(split.length > 1){
            int num = 1;
            long amt = -1;
            while(num < split.length){
                String token = split[num];
                if(number(token)){
                    amt = Long.parseLong(token);
                }
                if(token.toLowerCase().equals("all")){
                    amt = -1;
                }
                if(color(token)){
                    if(amt < 0){
                        if(UD.minerals.has(WordUtils.capitalizeFully(token))){
                            if(deposit(UD, token,UD.minerals.amountOf(WordUtils.capitalizeFully(token)))){
                                io.send("Deposited all "+token+" into the gate!",Snowflake.of(this.ID));
                            } else {
                                io.send("Failure to deposit???", Snowflake.of(this.ID));
                            }
                        } else {
                            io.send("You don't have any "+token+"!",Snowflake.of(this.ID));
                        }
                    } else {
                        if(deposit(UD, token,amt)){
                            io.send("Deposited "+amt+" "+token+" into the gate!",Snowflake.of(this.ID));
                        } else {
                                io.send("You don't have "+amt+" "+token+"!", Snowflake.of(this.ID));
                        }
                    }
                }
                
                num++;
            }
        } else {
        
            io.send("Enter the color you wish to deposit-eg deposit red 10 blue yellow all green 8 purple",Snowflake.of(this.ID));
        
        }
    }
    
    public boolean deposit(UserData UD, String color, long amt){
        
        color = WordUtils.capitalizeFully(color);
        
        if(!UD.minerals.has(color, amt)){
            return false;
        } else {
            UD.minerals.take(color, amt);
        }
        
        if(color.toLowerCase().equals("red")){
            minerals.give("Red",amt);
        }
        if(color.toLowerCase().equals("blue")){
            minerals.give("Blue",amt);
        }
        if(color.toLowerCase().equals("green")){
            minerals.give("Green",amt);
        }
        if(color.toLowerCase().equals("yellow")){
            minerals.give("Yellow",amt);
        }
        if(color.toLowerCase().equals("purple")){
            minerals.give("Purple",amt);
        }
        
        return true;
    }
    
    public boolean color(String s){
        String check = s.toLowerCase();
        if(check.equals("red") ||
                check.equals("blue")||
                check.equals("green") ||
                check.equals("yellow") ||
                check.equals("purple")){
            return true;
        }
        return false;
    }
    
    public boolean number(String s){
        try{
            long valid = Long.parseLong(s);
            return true;
        } catch (Exception E){
            return false;
        }
    }
    
    private void giveMin(Message mess){
        UserData UD = UserData.getUD(mess.getAuthor().get());
        String color = UD.depositing.toString();
        
        if(color.toLowerCase().equals("red")){
            UD.minerals.give("Red",mess.getContent().get().length());
        }
        if(color.toLowerCase().equals("blue")){
            UD.minerals.give("Blue",mess.getContent().get().length());
        }
        if(color.toLowerCase().equals("green")){
            UD.minerals.give("Green",mess.getContent().get().length());
        }
        if(color.toLowerCase().equals("yellow")){
            UD.minerals.give("Yellow",mess.getContent().get().length());
        }
        if(color.toLowerCase().equals("purple")){
            UD.minerals.give("Purple",mess.getContent().get().length());
        }
    }
    
    private void activateWeapon(UserData UD){
        int which = oRan.nextInt(100);
        String get = "";
        if(which < 10){
            get = UD.weaponC.toString();
        } else  if(which < 40){
            get = UD.weaponB.toString();
        } else  {
            get = UD.weaponA.toString();
        }
        
        if(ChatWeapon.getWeapon(get) != null){
            ChatWeapon.getWeapon(get).activate(this, UD);
        }
    }
    
    public String toString(){
        return "**GATE INFORMATION**\n"
                + "MINERALS: \n"
                + "```"+minerals+"```";
    }
    
}
