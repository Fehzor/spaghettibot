/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.progression;

import bot.main.IO;
import data.UserData;
import discord4j.core.object.util.Snowflake;
import java.util.ArrayList;

/**
 *
 * @author FF6EB
 */
public class Progress {
    public String ID;
    public String [] artifactID;
    public String text;
    
    public Progress(String ID, String text, String ... artIDs){
        this.ID = ID;
        this.text = text;
        this.artifactID = artIDs;
    }
    
    public boolean time(UserData UD){
        if(UD.artifacts.has(ID)){
            return false;
        } else if(artifactID == null){
            return true;
        }
        
        for(String id : artifactID){
            if(!UD.artifacts.has(id)){
                return false;
            } 
        }
        
        return true;
    }
    
    public void activate(UserData UD, IO io, Snowflake chanID){
        UD.artifacts.give(ID);
        
        io.send(text, chanID);
    }
    
    public static void checkProgress(UserData UD, IO io, Snowflake chanID){
        for(Progress P : progression){
            if(P.time(UD)){
                P.activate(UD, io, chanID);
                return;
            }
        }
    }
    
    private static ArrayList<Progress> progression = loadProgress();
    private static ArrayList<Progress> loadProgress(){
        ArrayList<Progress> ret = new ArrayList<>();
        
        String id = "tutA";
        String txt = "The swarm welcomes you to the planet of curdle where "
                + "things have gone sour as usual! Type >mine followed by "
                + "the color of your favorite mineral to start mining that "
                + "color. You'll mine 1 mineral of your set color (between red, "
                + "green, blue, yellow and purple) for each letter you type. To"
                + " see your minerals, type >min";
        
        
        ret.add(new Progress(id,txt,null));
        
        id = "tutB";
        txt = "Excellent work! Type >deposit followed by the color you mined to"
                + " add your minerals to the gate. Every so often the gate will"
                + " churn out a material for you to collect based on the colors"
                + " added! See the pins for more info.. or, type >mat to check"
                + " on what materials you have!";
        
        ret.add(new Progress(id,txt,"tutA"){
            public boolean time(UserData UD){
                if( !super.time(UD) ){
                    return false;
                }
                
                if(UD.minerals.has("Red",100)
                        ||UD.minerals.has("Blue",100)
                        ||UD.minerals.has("Green",100)
                        ||UD.minerals.has("Yellow",100)
                        ||UD.minerals.has("Purple",100)){
                    return true;
                }
                return false;
            }
        });
        
        id = "tutC";
        txt = "Whenever you're ready, type >craft to see what you can craft..."
                + " then, type >craft [item name] to craft it! >view [item] can"
                + " let you preview gear and >equip [slot] [item] equips the "
                + "item to slot A, B or C.";
        
        ret.add(new Progress(id,txt,"tutB"){
            public boolean time(UserData UD){
                if( !super.time(UD) ){
                    return false;
                }
                
                if(UD.materials.has("Beast",15)
                        ||UD.materials.has("Poison",15)||UD.materials.has("Fiend",15)
                        ||UD.materials.has("Undead",15)||UD.materials.has("Slime",15)
                        ||UD.materials.has("Construct",15)||UD.materials.has("Gremlin",15)
                        ||UD.materials.has("Shock",15)||UD.materials.has("Fire",15)
                        ||UD.materials.has("Freeze",15)
                        ){
                    return true;
                }
                return false;
            }
        });
        
        id = "tutD";
        txt = "Congratulations! You now know the basics of being on curdle! "
                + "You've earned yourself a Hatch Handle!";
        
        ret.add(new Progress(id,txt,"tutC"){
            public boolean time(UserData UD){
                if( !super.time(UD) ){
                    return false;
                }
                
                if(!((String)UD.weaponA.getData()).contains("Proto")
                        ||!((String)UD.weaponB.getData()).contains("Proto")
                        ||!((String)UD.weaponC.getData()).contains("Proto")){
                    return true;
                }
                return false;
            }
            
            public void activate(UserData UD, IO io, Snowflake chan){
                super.activate(UD,io,chan);
                
                UD.gear.give("Hatch Handle");
            }
        });
        
        return ret;
    }
}
