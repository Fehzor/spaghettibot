/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.chatweapons;

import static bot.SuperRandom.oRan;
import bot.command.CraftCom;
import data.UserData;
import java.util.HashMap;
import model.Gate;

/**
 *
 * @author FF6EB
 */
public class ChatWeapon {
    public String name;
    public int stars;
    public String [] mats;
    public String pre;
    
    public boolean secret = false;
    
    public String [] args;
    
    public ChatWeapon(String name, int stars, String [] code, String [] materials){
        this.pre=null;
        this.name = name;
        this.stars = stars;
        this.args = code;
        this.mats = materials;
    }
    
    public ChatWeapon(String pre, String name, int stars, String [] code, String [] materials){
        this(name,stars,code,materials);
        this.pre = pre;
    }
    
    private static HashMap<String, ChatWeapon> weapons = loadWeapons();
    private static HashMap<String, ChatWeapon> loadWeapons(){
        HashMap<String, ChatWeapon> ret = new HashMap<>();
        
        String req;
        String name;
        int stars;
        String [] args;
        String [] materials;
        
        materials = new String[]{};
        stars = 0;
        args = new String[]{"C1"};
        
        name = "Proto Sword";
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        name = "Proto Gun";
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        name = "Proto Bomb";
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        
        args = new String[]{"C10"};
        name = "Hatch Handle";
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        
        materials = new String[]{"Gremlin"};
        
        name = "Calibur";
        stars = 3;
        args = new String[]{"&BushyTail 3"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        req = name;
        name = "Tempered Calibur";
        stars = 3;
        args = new String[]{"&BushyTail 4"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Ascended Calibur";
        stars = 4;
        args = new String[]{"&BushyTail 5"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Leviathan Blade";
        stars = 5;
        args = new String[]{"&BushyTail 7"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Fiend","Undead","Gremlin","Freeze","Shock","Poison","Fire","BushyTail"};
        
        req = "Tempered Calibur";
        name = "Cold Iron Carver";
        stars = 4;
        args = new String[]{"&Undead 1"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Cold Iron Vanquisher";
        stars = 5;
        args = new String[]{"&Undead 2","&Curse 1"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Fiend","Undead","Krogmo","Curse","BushyTail"};
        
        req = "Cold Iron Vanquisher";
        name = "Arkus's Blade";
        stars = 5;
        args = new String[]{"C300","K10"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Slime"};
                
        name = "Flourish";
        stars = 2;
        args = new String[]{"K3"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        req = name;
        name = "Swift Flourish";
        stars = 3;
        args = new String[]{"K4"};
        ret.put(name, new ChatWeapon(req, name,stars,args,materials));
        req = name;
        name = "Grand Flourish";
        stars = 4;
        args = new String[]{"K5"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Final Flourish";
        stars = 5;
        args = new String[]{"K6"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Gremlin"};
                
        name = "Blaster";
        stars = 2;
        args = new String[]{"C10","K2"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        req = name;
        name = "Super Blaster";
        stars = 3;
        args = new String[]{"C50","K2"};
        ret.put(name, new ChatWeapon(req, name,stars,args,materials));
        req = name;
        name = "Master Blaster";
        stars = 4;
        args = new String[]{"C60","K3"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Valiance";
        stars = 5;
        args = new String[]{"C100","K4"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Slime","Stun"};
        
        req = "Flourish";
        name = "Rigadoon";
        stars = 3;
        args = new String[]{"C50","K3"};
        ret.put(name, new ChatWeapon(req, name,stars,args,materials));
        req = name;
        name = "Daring Rigadoon";
        stars = 4;
        args = new String[]{"C50","K3","O100"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Fearless Rigadoon";
        stars = 5;
        args = new String[]{"C100","K4","O150","M30"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Slime","Fire"};
        
        req = "Flourish";
        name = "Flamberge";
        stars = 3;
        args = new String[]{"O80","K3"};
        ret.put(name, new ChatWeapon(req, name,stars,args,materials));
        req = name;
        name = "Fierce Flamberge";
        stars = 4;
        args = new String[]{"C55","K5","O80"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Furious Flamberge";
        stars = 5;
        args = new String[]{"C80","K4","O50","M20"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Fire","Fiend","Gremlin","Beast"};
                
        name = "Shard Bomb";
        stars = 2;
        args = new String[]{"R10"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        req = name;
        name = "Super Shard Bomb";
        stars = 3;
        args = new String[]{"R20"};
        ret.put(name, new ChatWeapon(req, name,stars,args,materials));
        req = name;
        name = "Heavy Shard Bomb";
        stars = 4;
        args = new String[]{"R30"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Deadly Shard Bomb";
        stars = 5;
        args = new String[]{"R50"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Beast","Freeze","Slime","Poison"};
                
        name = "Crystal Bomb";
        stars = 2;
        args = new String[]{"G10"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        req = name;
        name = "Super Crystal Bomb";
        stars = 3;
        args = new String[]{"G20"};
        ret.put(name, new ChatWeapon(req, name,stars,args,materials));
        req = name;
        name = "Heavy Crystal Bomb";
        stars = 4;
        args = new String[]{"G30"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Deadly Crystal Bomb";
        stars = 5;
        args = new String[]{"G50"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Fire","Slime","Construct","Undead"};
                
        name = "Splinter Bomb";
        stars = 2;
        args = new String[]{"Y10"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        req = name;
        name = "Super Splinter Bomb";
        stars = 3;
        args = new String[]{"Y20"};
        ret.put(name, new ChatWeapon(req, name,stars,args,materials));
        req = name;
        name = "Heavy Splinter Bomb";
        stars = 4;
        args = new String[]{"Y30"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Deadly Splinter Bomb";
        stars = 5;
        args = new String[]{"Y50"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Fire","Shock","Poison","Freeze"};
        
        req = "Splinter Bomb";
        name = "Sun Shards";
        stars = 3;
        args = new String[]{"R4","G4","B4","Y4","P4"};
        ret.put(name, new ChatWeapon(req, name,stars,args,materials));
        req = name;
        name = "Radiant Sun Shards";
        stars = 4;
        args = new String[]{"R6","G6","B6","Y6","P6"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Scintillating Sun Shards";
        stars = 5;
        args = new String[]{"R10","G10","B10","Y10","P10"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Fiend","Undead","Shock","Poison"};
        
        name = "Dark Matter Bomb";
        stars = 2;
        args = new String[]{"Y10"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        req = name;
        name = "Super Dark Matter Bomb";
        stars = 3;
        args = new String[]{"Y20"};
        ret.put(name, new ChatWeapon(req, name,stars,args,materials));
        req = name;
        name = "Heavy Dark Matter Bomb";
        stars = 4;
        args = new String[]{"Y30"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Deadly Dark Matter Bomb";
        stars = 5;
        args = new String[]{"Y50"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Shock","Construct","Gremlin","Freeze"};
        
        req = "Dark Matter Bomb";
        name = "Salt Bomb";
        stars = 3;
        args = new String[]{"B20"};
        ret.put(name, new ChatWeapon(req, name,stars,args,materials));
        req = name;
        name = "Ionized Salt Bomb";
        stars = 4;
        args = new String[]{"B30"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Shocking Salt Bomb";
        stars = 5;
        args = new String[]{"B50"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Construct"};
                
        name = "Brandish";
        stars = 2;
        args = new String[]{"C10"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        
        materials = new String[]{"Construct","Fire"};
        
        req = name;
        name = "Fireburst Brandish";
        stars = 3;
        args = new String[]{"C20","M100"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Blazebrand";
        stars = 4;
        args = new String[]{"C40","M70"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        materials = new String[]{"Construct","Fire","BushyTail"};
        req = name;
        name = "Combuster";
        stars = 5;
        args = new String[]{"C80","M50"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Construct","Fiend","Undead","Fire"};
        
        req = "Brandish";
        name = "Nightblade";
        stars = 3;
        args = new String[]{"M7"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Silent Nightblade";
        stars = 4;
        args = new String[]{"M3"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Construct","Fiend","Undead","Fire","Curse"};
        req = name;
        name = "Acheron";
        stars = 5;
        args = new String[]{"M1"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Construct","Freeze"};
        
        req = "Brandish";
        name = "Iceburst Brandish";
        stars = 3;
        args = new String[]{"C10","M8"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Blizzbrand";
        stars = 4;
        args = new String[]{"C15","M6"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Construct","Freeze","BushyTail"};
        req = name;
        name = "Glacius";
        stars = 5;
        args = new String[]{"C30","M3"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Construct","Shock","Krogmo"};
        
        req = "Brandish";
        name = "Shockburst Brandish";
        stars = 3;
        args = new String[]{"C15","M9"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Boltbrand";
        stars = 4;
        args = new String[]{"C25","M7"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Voltedge";
        stars = 5;
        args = new String[]{"C60","M4"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        
        materials = new String[]{"Beast","Poison"};
                
        name = "Cutter";
        stars = 2;
        args = new String[]{"C10"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        req = name;
        name = "Striker";
        stars = 3;
        args = new String[]{"C50"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Construct","Poison"};
        
        req = name;
        name = "Vile Striker";
        stars = 4;
        args = new String[]{"C100"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Dread Venom Striker";
        stars = 5;
        args = new String[]{"C300"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Beast","Slime","BushyTail"};
        
        req = "Striker";
        name = "Hunting Blade";
        stars = 4;
        args = new String[]{"&BushyTail 4","W1000"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Wild Hunting Blade";
        stars = 5;
        args = new String[]{"&BushyTail 6","W600"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Slime","Beast"};
                
        name = "Autogun";
        stars = 2;
        args = new String[]{"C10"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        req = name;
        name = "Needle Shot";
        stars = 3;
        args = new String[]{"C50"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Strike Needle";
        stars = 4;
        args = new String[]{"C100"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Blitz Needle";
        stars = 5;
        args = new String[]{"C300"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        materials = new String[]{"Poison","Krogmo"};
        
        req = "Auto Gun";
        name = "Toxic Shot";
        stars = 3;
        args = new String[]{"C30","O30"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Blight Needle";
        stars = 4;
        args = new String[]{"C70","O70"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        req = name;
        name = "Plague Needle";
        stars = 5;
        args = new String[]{"C200","O200"};
        ret.put(name, new ChatWeapon(req,name,stars,args,materials));
        
        
        materials = new String[]{};
        
        name = "Kadalborag";
        stars = 5;
        args = new String[]{"R11","G11","B11","Y11","P11"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        name = "Phantom Striker";
        stars = 5;
        args = new String[]{"C13","M13"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        name = "Ranger Signal Flare";
        stars = 5;
        args = new String[]{"N","N"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
                
        name = "Caladbolg";
        stars = 5;
        args = new String[]{"R11","G11","B11","Y11","P11"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        name = "Overcharged Mixmaster";
        stars = 5;
        args = new String[]{"O500"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        name = "Celestial Orbitgun";
        stars = 5;
        args = new String[]{"O500"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        name = "Rock Salt";
        stars = 5;
        args = new String[]{"&Slime 12"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        stars = 5;
        args = new String[]{"D300"};
        name = "Cool Owlite Wand";
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        name = "Heavy Owlite Wand";
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        name = "Divine Owlite Wand";
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        name = "Mod Calibrator";
        stars = 5;
        args = new String[]{"C11","N"};
        ret.put(name, new ChatWeapon(name,stars,args,materials));
        
        ret.put("Mug Of Misery",new MoM());
        ret.put("Mecha Knight Kit",new MechaKit());
        
        return ret;
    }
    
    public static ChatWeapon getWeapon(String weapon){
        return weapons.get(weapon);
    }
    
    public static ChatWeapon [] getAllWeapons(){
        ChatWeapon [] ret = new ChatWeapon [weapons.size()];
        
        int i = 0;
        for(ChatWeapon CW : weapons.values()){
            ret[i] = CW;
            i++;
        }
        
        return ret;
    }
    
    public void activate(Gate G, UserData user){
        UserData UD;
        for(String arg : args){
            String tail = arg.substring(1);
            switch (arg.charAt(0)){
                case 'R':
                    G.minerals.give("Red",Integer.parseInt(tail));
                    break;
                case 'G':
                    G.minerals.give("Green",Integer.parseInt(tail));
                    break;
                case 'B':
                    G.minerals.give("Blue",Integer.parseInt(tail));
                    break;    
                case 'Y':
                    G.minerals.give("Yellow",Integer.parseInt(tail));
                    break;
                case 'P':
                    G.minerals.give("Purple",Integer.parseInt(tail));
                    break;
                case 'C':
                    user.crowns.append(Integer.parseInt(tail));
                    break;
                case 'M':
                    if(oRan.nextInt(Integer.parseInt(tail))==0){
                        G.gevent.execute(user);
                    }
                    break;
                case 'W':
                    if(oRan.nextInt(Integer.parseInt(tail)) == 0){
                        user.materials.give("Beast",1000);
                    }
                    break;
                case 'O':
                    UD = G.gevent.randomUser();
                    UD.crowns.append(Long.parseLong(tail));
                    break;
                case 'D':
                    if(user.crowns.getData() >= Long.parseLong(tail)){
                        user.crowns.append(Long.parseLong(tail) * -1);
                        for(int i = 0; i < 5; ++i){
                            UD = G.gevent.randomUser();
                            if(UD != user){
                                UD.crowns.append(Long.parseLong(tail));
                            }
                        }
                    }
                    break;
                case 'K':
                    user.materials.give("Krogmo",Long.parseLong(tail));
                    break;
                case '&':
                    long num = Long.parseLong(tail.split(" ")[1]);
                    String what = tail.split(" ")[0].substring(0);
                    user.materials.give(what,num);
                    break;
                case 'N':
                    ChatWeapon CW = ChatWeapon.getWeapon(user.weaponA.toString());
                    if(!CW.name.equals(this.name)){
                        CW.activate(G, user);
                    }
                    break;
            }
        }
    }
    
    public String toString(){
        String ret = "**"+this.name+"**\n";
        ChatWeapon cw = this;
        String pres = "";
        while(cw.pre != null && cw.pre != ""){
            cw = getWeapon(cw.pre);
        }
        
        
        ret+="Crafting Tree: \n```\n"+getUpgrades(cw.name,0)+"```\n";
        
        if(this.mats.length != 0){
            ret+="Crafting costs: "+CraftCom.craftingCost(this.stars)+"x [";
            for(String S : this.mats){
                ret+=S+"; ";
            }
            ret+="]\n";
        }
        
        for(String arg : args){
            String tail = arg.substring(1);
            ret+="On Use: ";
            switch (arg.charAt(0)){
                case 'R':
                    ret+="Adds "+tail+" red to the gate!";
                    break;
                case 'G':
                    ret+="Adds "+tail+" green to the gate!";
                    break;
                case 'B':
                    ret+="Adds "+tail+" blue to the gate!";
                    break;    
                case 'Y':
                    ret+="Adds "+tail+" yellow to the gate!";
                    break;
                case 'P':
                    ret+="Adds "+tail+" purple to the gate!";
                    break;
                case 'C':
                    ret+="Grants the user "+tail+" crowns!";
                    break;
                case 'M':
                    ret+="1 in "+tail+" chance to gain a material!";
                    break;
                case 'W':
                    ret+="1 in "+tail+" chance to MURDERIZE BEASTS.";
                    break;
                case 'O':
                    ret+="Grants a random user (possibly the owner) "+tail+" crowns!";
                    break;
                case 'D':
                    ret+="Donates "+tail+" of the users's crowns to up to 5 other people! (up to 5x the crowns generated!)";
                    break;
                case 'K':
                    ret+="Grants the user "+tail+" Krogmo Coins (material)!";
                    break;
                case '&':
                    long num = Long.parseLong(tail.split(" ")[1]);
                    String what = tail.split(" ")[0].substring(0);
                    ret+="Grants the user "+num+" "+what+" materials!";
                    break;
                case 'N':
                    ret+="Activates whatever is in slot A... but not itself.";
                    break;
                default:
                    ret+="???";
                    break;
            }
            ret+="\n";
        }
        
        return ret;
    }
    
    public static String getUpgrades(String name, int tabs){
        String ret = name+"\n";
        for(int i = 0; i < tabs; ++i){
            ret = "\t"+ret;
        }
        for(ChatWeapon nex : weapons.values()){
            if(nex.secret != true && nex.pre != null && nex.pre.equals(name)){
                ret+=getUpgrades(nex.name,tabs+1);
            }
        }
        return ret;
    }
}
