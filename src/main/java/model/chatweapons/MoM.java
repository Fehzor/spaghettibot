/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.chatweapons;

import data.Field;
import data.MapField;
import data.UserData;
import discord4j.core.object.util.Snowflake;
import java.util.ArrayList;
import model.Gate;

/**
 *
 * @author FF6EB
 */
public class MoM extends ChatWeapon{
    
    private MapField<Long> tears;
    private MapField<Long> devilites;
    
    
    
    public MoM() {
        super("Mug Of Misery", 5, new String[]{""}, new String[]{""});
        tears = new MapField<>("MoM","Tears");
        devilites = new MapField<>("MoM","Devilites");
    }
    
    public void activate(Gate G, UserData user){
        String name = user.user.getUsername();
        
        if(tears.get(user.snow.asString()) != null){
            tears.append(user.snow.asString(), 1l);
        } else {
            G.io.send(name+" begins to cry into the mug... (tears +1 per message!)", Snowflake.of(G.ID));
            tears.put(user.snow.asString(),1l);
            devilites.put(user.snow.asString(), 0l);
        }
        
        long num = devilites.get(user.snow.asString());
        
        if(tears.get(user.snow.asString()) >= nextLevel(num)){
            tears.put(user.snow.asString(), 0l);
            G.io.send(getMess(name, num), Snowflake.of(G.ID));
            devilites.append(user.snow.asString(), 1l);
        }
        
        if(num > 0){
            user.crowns.append(num * 20);
        }
    }
    
    private String getMess(String name, long devils){
        if(devils<20){
            int swit = (int)devils;
            switch(swit){
                case 0:
                    return name+"'s body sweats and a strange thirst comes over "
                            + "them.. "+name+" drinks from the mug of misery!"
                            + " A tiny devilite wanders out of the darkness..."
                            + " The little fellow gets to work almost immediatly"
                            + " providing "+name+" with crowns (+1 devilite! "
                            + "20 cr/devilite/message!)";
                case 1:
                    return name+"'s mug feels heavier this time than it did the last."
                            + " The urge however feels the same. (+1 devilite)";
                case 2:
                    return name+"'s mug of misery shakes violently from time to "
                            + "time as if something is stirring in the blackness"
                            + "that engulfs the bottom of the cup. (+1 devilite!)";
                case 3:
                    return name+" feels great pain as they drink the liquid from"
                            + "their mug of misery. The greedy feeling overpowers"
                            + "the salty taste of their own tears. (+1 devilite!)";
                case 4:
                    return name+"'s eyes hurt from crying, but the mug calls"
                            + "to "+name+" from a strange place, far away.."
                            + " (+1 devilite!)";
                case 5:
                    return "The sound of "+name+"'s sobs calm them as they amass"
                            + "more and more workers. (+1 devilites)";
                case 6:
                    return "The pain and sorrow "+name+" feels is unbearable. "
                            + name+" wants to die. (+1 devilite!)";
                case 7:
                    return name+" tried to pour out the mug, but can never be "
                            + "free of its burden. "
                            + name+" can never stop filling "
                            + "the glass, always half empty, never half full."
                            + "(+1 devilite!)";
                case 8:
                    return name+" realizes that since filling the mug of misery"
                            + " they have been without ambition, "
                            + "without dreams, and without purpose."
                            + " (+1 devilite!)";
                case 9:
                    return name+" has begun to organize themselves in a more"
                            + " timely fashion, creating synergy in their"
                            + " workplace. (+1 devilite)";
                case 10:
                    return "It is important to realize that leaders are often "
                            + "correct in the workplace. They got there for a "
                            + "reason. They shed blood, sweat, and most "
                            + "importantly... tears. (+1 devilite for "+name+")";
                case 11:
                    return name+" knows that speed matters in business. "
                            + "That many decisions and actions are reversible "
                            + "and do not need extensive study. "+name+" values "
                            + "calculated risk taking. (+1 devilite)";
                case 12:
                    return name+" thinks like an owner. Thinks in the long term. "
                            + name + " knows not to sacrifice long term value for "
                            + "short term results. All of "+name+"'s devilites "
                            + "are taught this as they emerge as well. These "
                            + "workers never say \"that's not my job!\" (+1 devilite!)";
                case 13:
                    return "A new devilite emerges as "+name+" drinks the "
                            + "black syrup that is their tears. The devilite "
                            + "must first be branded in "+name+"'s name, trained "
                            + "and most importantly, disciplined. "+name+" does"
                            + " all of this the moment the new fellow is born. "
                            + "(+1 devilite!)";
                case 14:
                    return "When workers disobey "+name+", "+name+" throws things"
                    + " at them. The threat of becoming a gorgo looms. No one"
                            + "wants to resemble a flying testicle. (+1 devilite!)";
                case 15:
                    return "Devilites that start unions are known as gorgos. (+1 devilite for "+name+")";
                case 16:
                    return name+" organizes their devilites well. There are "
                            + "new cover sheets on the TPS reports. The red"
                            + " stapler shines brighter than the sun. Profits "
                            + "are higher than ever before. (+1 devilite!)";
                case 17:
                    return name+" finds it better to fire people on a Friday. "
                            + "Studies ahve statistically shown that there's less"
                            + " chance of an incident if you do it at the end of "
                            + "the week... (-3 devilites, +4 new devilites!)";
                case 18:
                    return name+"'s mug of misery weighs on them like a million"
                            + " pounds of concrete. A million wasted seconds. "
                            + "Wasted youth, a wasted world. "+name+" feels "
                            + "nothing. "+name+" is the product of a wasted "
                            + "world, just as their workers are the waste "
                            + "of that world. (+1 devilite)";
                case 19:
                    return name+"'s bald head shines brightly, their hair having"
                            + "fallen out months earlier. "+name+" hasn't smiled"
                            + " in lifetimes. "+name+" would want to die if "
                            + name+" was still an optimist. (+1 devilite)";
            }
        }
        return "On days like this, "+name+" should be burning in hell...\n\n"
                + "(devilites +1!)";
    }
    
    private long nextLevel(long devils){
        if(devils<20){
            int swit = (int)devils;
            switch(swit){
                case 0:
                    return 13;
                case 1:
                    return 169;
                case 2:
                    return 200;
                case 3:
                    return 250;
                case 4:
                    return 300;
                case 5:
                    return 400;
                case 6:
                    return 500;
                case 7:
                    return 700;
                case 8:
                    return 900;
                case 9:
                    return 1200;
                case 10:
                    return 1500;
                case 11:
                    return 1800;
                case 12:
                    return 2500;
                case 13:
                    return 3000;
                case 14:
                    return 3500;
                case 15:
                    return 4000;
                case 16:
                    return 5000;
                case 17:
                    return 6000;
                case 18:
                    return 8000;
                case 19:
                    return 10000;
            }
        }
        return 13000;
    }
    
    public String toString(){
        return "???";
    }
}
