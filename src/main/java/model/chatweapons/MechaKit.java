/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.chatweapons;

import data.Field;
import data.MapField;
import data.UserData;
import discord4j.core.object.util.Snowflake;
import java.util.ArrayList;
import model.Gate;

/**
 *
 * @author FF6EB
 */
public class MechaKit extends ChatWeapon{
    
    private MapField<Long> storyNum;
    
    
    
    public MechaKit() {
        super("Mecha Knight Kit", 5, new String[]{""}, new String[]{""});
        storyNum = new MapField<>("MKK","Stories");
    }
    
    public void activate(Gate G, UserData user){
        String name = user.user.getUsername();
        
        
        
        if(storyNum.get(user.snow.asString()) != null){
            long story = storyNum.get(user.snow.asString());
            long amt = getAmt(story);
        
            if(story < 1000 && user.materials.has("Construct",amt)){
                user.materials.take("Construct",amt);
                storyNum.append(user.snow.asString(), 1l);
                String mess = getMess(name, story);
                if(mess != null && !mess.equals("")){
                    G.io.send(mess, Snowflake.of(G.ID));
                }
            }
            if(story >= 1000){
                G.gevent.execute(user);
                user.crowns.append(500l);
            }
            
        } else {
            G.io.send(name+" used their mecha knight kit! It.. acted as a brief"
                    + " distraction and died. Boooooooooooo. Perhaps something "
                    + "else could be done with the kit??? You have a feeling "
                    + "like you're going to need a lot of construct materials.."
                    + "...", Snowflake.of(G.ID));
            storyNum.put(user.snow.asString(),0l);
        }
        
    }
    
    private String getMess(String name, long round){

            int swit = (int)round;
            switch(swit){
                case 10:
                    return name+" has begun gathering construct materials. But "
                            + "what to do with them? There are so many "
                            + "possibilities...";
                case 30:
                    return name+" has built.. a gun puppy. It dies quickly, "
                            + "just as the mecha knight did. What a waste.";
                case 50:
                    return name+" continues to save construct materials. (50 saved!)";
                case 100:
                    return name+" begins saving constuct materials faster. "
                            + "What will they create?";
                case 200:
                    return name+" found some sort of blue print when rading "
                            + "a devilite area. They begin saving constuct "
                            + "materials even faster. ";
                    
                case 250:
                    return name+" begins constructing a tortodrone using their "
                            + "mecha knight kit!";
                case 300:
                    return name+" begins saving constuct materials faster. "
                            + "The tortodrone grows in size. More materials!";
                case 400:
                    return "Building a tortodrone is taking a lot longer than "
                            + name +" thought it would...";
                case 500:   
                    return name+" ramps up the amount of tortodrone materials "
                            + "they're hoarding once again. ";
                case 800:
                    return name+" ramps up the number of tortodrone materials "
                            + "saved one last time. This better be worth it.";
                case 1000:
                    return name+" has built a tortodrone!!! It boots up and "
                            + "begins hunting for something... leaving a path"
                            + " of destruction in its wake for you to pick up!"
                            + " (+500 cr, 1 mat per message!)";
                    
                
            }
        return "";
    }
    
    private long getAmt(long round){
        if(round == 0){
            return 0;
        } else if(round <= 100){
            return 3;
        } else if(round <= 200){
            return 7;
        } else if(round <= 300){
            return 20;
        } else if(round <= 500){
            return 50;
        } else if(round <= 800){
            return 200;
        } else if(round <= 1000){
            return 500;
        } else {
            return 0;
        }
    }
    
    public String toString(){
        return "???";
    }
}
