/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import bot.main.IO;
import data.InventoryField;
import data.UserData;
import model.chatweapons.ChatWeapon;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author FF6EB
 */
public class Commons {
    public static InventoryField auction;
    
    public static IO io;
    
    public static final long MAT_PRICE = 10;
    public static final long TWO_PRICE = 500;
    public static final long THREE_PRICE = 12000;
    public static final long FOUR_PRICE = 100000;
    public static final long FIVE_PRICE = 800000;
    
    public static final double BUY_BACK = 1.5;
    
    public Commons(IO io){
        this.io = io;
        
        auction = new InventoryField("auction","house");   
    }
    
    public static String buy(UserData ud, String item, long amt){
        if(auction.has(WordUtils.capitalizeFully(item),amt)){
             ChatWeapon CW = ChatWeapon.getWeapon(WordUtils.capitalizeFully(item));
             if(CW != null){
                 if(CW.stars == 2){
                     if(ud.crowns.getData()>=TWO_PRICE*BUY_BACK){
                         ud.crowns.append((long) (-TWO_PRICE*BUY_BACK));
                     } else {
                         return "You can't afford that!";
                     }
                     
                     ud.gear.give(WordUtils.capitalizeFully(item));
                     auction.take(WordUtils.capitalizeFully(item));
                     
                     return "You buy one for 10K cr!";
                }
                 if(CW.stars == 3){
                     if(ud.crowns.getData()>=THREE_PRICE*BUY_BACK){
                         ud.crowns.append((long) (-THREE_PRICE*BUY_BACK));
                     } else {
                         return "You can't afford that!";
                     }
                     
                     ud.gear.give(WordUtils.capitalizeFully(item));
                     auction.take(WordUtils.capitalizeFully(item));
                     
                     return "You buy one for 30K cr!";
                }
                
                 if(CW.stars == 4){
                     if(ud.crowns.getData()>=FOUR_PRICE*BUY_BACK){
                         ud.crowns.append((long) (-FOUR_PRICE*BUY_BACK));
                     } else {
                         return "You can't afford that!";
                     }
                     
                     ud.gear.give(WordUtils.capitalizeFully(item));
                     auction.take(WordUtils.capitalizeFully(item));
                     
                     return "You buy one for 160K cr!";
                }
                 
                if(CW.stars == 5){
                     if(ud.crowns.getData()>=FIVE_PRICE*BUY_BACK){
                         ud.crowns.append((long) (-FIVE_PRICE*BUY_BACK));
                     } else {
                         return "You can't afford that!";
                     }
                     
                     ud.gear.give(WordUtils.capitalizeFully(item));
                     auction.take(WordUtils.capitalizeFully(item));
                     
                     return "You buy one for "+FIVE_PRICE*BUY_BACK+" cr!";
                }
             } else {
                 if(item.toLowerCase().equals("construct") ||
                    item.toLowerCase().equals("beast") ||
                    item.toLowerCase().equals("slime") ||
                    item.toLowerCase().equals("undead") ||
                    item.toLowerCase().equals("gremlin") ||
                    item.toLowerCase().equals("fiend") ||
                    item.toLowerCase().equals("fire") ||
                    item.toLowerCase().equals("shock") ||
                    item.toLowerCase().equals("freeze") ||
                    item.toLowerCase().equals("poison")){
                     
                long price = (long) (amt * MAT_PRICE * BUY_BACK);
                if(ud.crowns.getData()>=price){
                    ud.crowns.append(-price);
                } else {
                    return "You can't afford that or in that quantity!";
                }

                ud.materials.give(WordUtils.capitalizeFully(item), amt);
                auction.take(WordUtils.capitalizeFully(item));

                return "You buy them for "+amt*MAT_PRICE*BUY_BACK+" cr!";
            }
             }
        }
        return "That isn't on the auction house (or in that quantity). Try again later or don't typo!";
    }
    
    public static String sell(UserData ud, String item, long amt){
        ChatWeapon CW = ChatWeapon.getWeapon(WordUtils.capitalizeFully(item));
        if(CW == null){
            if(item.toLowerCase().equals("construct") ||
                    item.toLowerCase().equals("beast") ||
                    item.toLowerCase().equals("slime") ||
                    item.toLowerCase().equals("undead") ||
                    item.toLowerCase().equals("gremlin") ||
                    item.toLowerCase().equals("fiend") ||
                    item.toLowerCase().equals("fire") ||
                    item.toLowerCase().equals("shock") ||
                    item.toLowerCase().equals("freeze") ||
                    item.toLowerCase().equals("poison")){
                if(ud.materials.has(WordUtils.capitalizeFully(item), amt)){
                    ud.materials.take(WordUtils.capitalizeFully(item), amt);
                } else {
                    return "You don't have that many "+item+"!";
                }
                auction.give(WordUtils.capitalizeFully(item),amt);
                ud.materials.take(item,amt);
                ud.crowns.append(amt*MAT_PRICE);
                return "You sell "+amt+" "+item+" materials for "+amt*MAT_PRICE+" crowns..";
            }
            
            return "The strangers don't seem interested in buying that from you...";
        } else {
            if(CW.toString().equals("???")){
                return "The strangers don't seem interested in buying that from you...";
            }
            
            if(ud.gear.has(WordUtils.capitalizeFully(item))){
                ud.gear.take(WordUtils.capitalizeFully(item));
            } else {
                return "You don't have "+item+"!";
            }

            if(CW.stars == 2){
                auction.give(WordUtils.capitalizeFully(item));
                ud.crowns.append(TWO_PRICE);
                return "The strangers buy your item for "+TWO_PRICE+" crowns...";
            }

            if(CW.stars == 3){
                auction.give(WordUtils.capitalizeFully(item));
                ud.crowns.append(THREE_PRICE);
                return "The strangers buy your item for "+THREE_PRICE+" crowns...";
            }

            if(CW.stars == 4){
                auction.give(WordUtils.capitalizeFully(item));
                ud.crowns.append(FOUR_PRICE);
                return "The strangers buy your item for "+FOUR_PRICE+" crowns...";
            }

            if(CW.stars == 5){
                auction.give(WordUtils.capitalizeFully(item));
                ud.crowns.append(FIVE_PRICE);
                return "The strangers buy your item for "+FIVE_PRICE+" crowns...";
            }
        }
        
        return "The strangers don't seem interested in buying that from you...";
    }
}
