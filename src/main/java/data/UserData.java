/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import bot.main.IO;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author FF6EB
 */
public class UserData {
    //private static Field<ArrayList<Long>> IDList = new Field<ArrayList<Long>>(new ArrayList<Long>(),"USERDATA","IDLIST");
    private static HashMap<Long,UserData> UserList = new HashMap<>();
    
    private static ReentrantLock lock = new ReentrantLock();
    
    public static UserData getUD(User user){
        while(!lock.tryLock()){}
        long ID = user.getId().asLong();
        
        if(!UserList.containsKey(ID)){
            //if(!IDList.getData().contains(ID)){
            //    IDList.getData().add(ID);
            //}
            UserList.put(ID,new UserData(user));
        }
        
        //IDList.write();
        
        //if(UserList.get(user.getID()).name.equals("Clint Eastwood's Character")){
        //    UserList.get(user.getID()).name = user.getName();
        //}
        
        lock.unlock();
        
        return UserList.get(ID);
    }
    
    public static UserData getUD(IO io, Snowflake ID){
        return getUD(io.client.getUserById(ID).block());
    }
    
    public Snowflake snow;
    public User user;
    
    public StringField SKName;
    public StringField SKGuild;
    
    public NumberField messages;
    public NumberField letters;
    public NumberField crowns;
    
    public StringField depositing;
    
    public InventoryField materials;
    public InventoryField gear;
    public InventoryField artifacts;
    public InventoryField minerals;
    public InventoryField costumes;
    
    public StringField weaponA;
    public StringField weaponB;
    public StringField weaponC;
    
    private UserData(User user){
        this.snow = user.getId();
        this.user = user;
        
        SKName = new StringField(snow.asString(),"SK","Name");
        if(SKName.empty()){
            SKName.set("Unspecified");
        }
        
        SKGuild = new StringField(snow.asString(),"SK","Guild");
        if(SKGuild.empty()){
            SKGuild.set("Unspecified");
        }
        
        messages = new NumberField(snow.asString(),"messages");
        letters = new NumberField(snow.asString(),"letters");
        
        crowns = new NumberField(snow.asString(), "dressing");
        
        depositing = new StringField(snow.asString(), "deposit");
        if(depositing.empty()){
            depositing.set("None");
        }
        
        artifacts = new InventoryField(snow.asString(),"artifacts");
        materials = new InventoryField(snow.asString(),"mats");
        minerals = new InventoryField(snow.asString(),"mins");
        costumes = new InventoryField(snow.asString(),"costumes");
        gear = new InventoryField(snow.asString(),"gear");
        
        
        weaponA = new StringField(snow.asString(),"WA");
        weaponB = new StringField(snow.asString(),"WB");
        weaponC = new StringField(snow.asString(),"WC");
        if(weaponA.empty()){
            weaponA.set("Proto Sword");
        }
        if(weaponB.empty()){
            weaponB.set("Proto Gun");
        }
        if(weaponC.empty()){
            weaponC.set("Proto Bomb");
        }
    }
    
    
    
    public String info(){
        String username = "**Discord Handle: ** "+user.getUsername()+"#"+user.getDiscriminator()+"\n";
        String name = "**Knight Name: **"+SKName+"\n";
        String guild = "**Guild: **"+SKGuild+"\n";
        
        String otherstats = "\n**Statistics**\n```";
        otherstats += "Messages Sent: "+messages.getData()+"\n";
        otherstats += "Characters Sent: "+letters.getData()+"\n";
        if(messages.getData() > 0){
            otherstats += "Average Characters per Message: "+(((double)(letters.getData())) / ((double)(messages.getData())))+"\n";
        }
        otherstats+="```";        
        
        
        
        
        
        
        String ret = username+name+guild+otherstats;
        
        return ret;
    }
    
    public String inv(){
        String ret = "";
        
        String username = "**Inventory Of: ** "+user.getUsername()+"#"+user.getDiscriminator()+"/"+this.SKName.toString()+"\n";
        
        String money = "\n**Crowns: **"+crowns+"\n";
        String deposit = "**Mining:** "+this.depositing.toString()+"\n";
        
        String weapons = "\n**Weapons (one will activate at random each time you chat!)**\n";
        weapons+= "Weapon A (60%): "+weaponA.toString()+"\n";
        weapons+= "Weapon B (30%): "+weaponB.toString()+"\n";
        weapons+= "Weapon C (10%): "+weaponC.toString()+"\n\n";
        
        String gears = "**Gear**\n";
        if(gear.isEmpty()){
            gears += "```None?```\n";
        } else {
            gears+= "```"+gear.toString()+"```";
        }
        
        
        ret = username + money + deposit + weapons + gears;
        
        return ret;
    }
    
    public String toString(){
        return "ud_"+this.snow.asLong();
    }
    
    
}
