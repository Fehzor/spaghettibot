/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import bot.main.IO;
import data.UserData;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;
import java.util.HashMap;

/**
 *
 * @author FF6EB
 */
public class TimeOutEvent extends TimeEvent{
    
    public long timeOutTime;
    
    public HashMap<UserData, Long> lastMessage = new HashMap<>();
    
    public TimeOutEvent(IO io, long timer, long timeout, String name) {
        super(io, timer, name);
        
        timeOutTime = timeout;
    }
    
    public void ping(UserData UD){
        lastMessage.put(UD,System.currentTimeMillis());
        
        if(!IDList.getData().contains(UD.snow.asLong())){
            this.addUser(UD.user);
        }
    }
    
    public void run(){
        while(running){
            Thread.yield();
            
            if(System.currentTimeMillis() - last > timer){
                for(int i = IDList.getData().size()-1; i >= 0; --i){
                    long ID = IDList.getData().get(i);
                    
                    Snowflake snow = Snowflake.of(ID);
                    User u = io.client.getUserById(snow).block();
                    UserData UD = UserData.getUD(u);
                    try{
                        execute(UD);
                    } catch (Exception E){
                        E.printStackTrace();
                    }
                    last = System.currentTimeMillis();
                    
                    try{
                        if(lastMessage.get(UD) - last > timeOutTime){
                            this.removeUser(UD);
                        }
                    } catch (Exception E){}
                }
                
            }
        }
    }
    
    public void addUser(User U){
        super.addUser(U);
        lastMessage.put(UserData.getUD(U),System.currentTimeMillis());
    }
}
