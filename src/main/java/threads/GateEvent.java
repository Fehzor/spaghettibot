/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import static bot.SuperRandom.oRan;
import bot.main.IO;
import data.InventoryField;
import data.UserData;
import discord4j.core.object.entity.Channel;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.util.Snowflake;
import model.Gate;
import model.progression.Progress;

/**
 *
 * @author FF6EB
 */
public class GateEvent extends TimeOutEvent{
    private static final long TIME = 1000 * 10 ; //1 tick = 10 seconds for now
    private static final long TIME_OUT = 1000 * 60 * 20; //20 minutes and you're out!'
    private static final double PERCENT = .01; //Takes % per material given.
    
    Gate G;
    
    
    
    public GateEvent(IO io, Gate gate){
        super(io, TIME, TIME_OUT, "gate "+gate.ID);
        this.G = gate;
    }
    
    public void execute(UserData UD){
        Progress.checkProgress(UD, io, Snowflake.of(G.ID));
        
        String get = getMaterial(G.minerals);
        if(get==null){
            return;
        }
        
        long amt = Long.parseLong(get.split(" ",2)[0]);
        amt = amt / 2;
        
        get= get.split(" ",2)[1];
        
        UD.materials.give(get, amt);
    }
    
    private String getMaterial(InventoryField minerals){
        String A = minerals.randomWeightedItem();
        String B = minerals.randomWeightedItem();
        if(A == null){
            return null;
        }
        if(B == null){
            return null;
        }
        int trials = 100;
        while(A.equals(B) && trials > 0){
            B = minerals.randomWeightedItem();
            trials = trials - 1;
        }
        if(B == null){
            return null;
        }
        
        int answer = 0;
        long amt = 0;
        if(A.toLowerCase().equals("red") || B.toLowerCase().equals("red")){
            answer+=1;
            amt += takeMineral(minerals,"Red");
        }
        if(A.toLowerCase().equals("green") || B.toLowerCase().equals("green")){
            answer+=2;
            amt += takeMineral(minerals,"Green");
        }
        if(A.toLowerCase().equals("blue") || B.toLowerCase().equals("blue")){
            answer+=4;
            amt += takeMineral(minerals,"Blue");
        }
        if(A.toLowerCase().equals("yellow") || B.toLowerCase().equals("yellow")){
            answer+=8;
            amt += takeMineral(minerals,"Yellow");
        }
        if(A.toLowerCase().equals("purple") || B.toLowerCase().equals("purple")){
            answer+=16;
            amt += takeMineral(minerals,"Purple");
        }
        
        if(answer == 3){
            return amt+" Beast";
        }
        if(answer == 5){
            return amt+" Gremlin";
        }
        if(answer == 6){
            return amt+" Freeze";
        }
        if(answer == 9){
            return amt+" Fire";
        }
        if(answer == 10){
            return amt+" Slime";
        }
        if(answer == 12){
            return amt+" Construct";
        }
        if(answer == 17){
            return amt+" Fiend";
        }
        if(answer == 18){
            return amt+" Poison";
        }
        if(answer == 20){
            return amt+" Shock";
        }
        if(answer == 24){
            return amt+" Undead";
        }
        
        return null;
    }
    
    private long takeMineral(InventoryField minerals, String mineral){
        if(!minerals.has(mineral)){
            return 0;
        }
        long tax = (long)Math.ceil(minerals.amountOf(mineral)*PERCENT);
        minerals.take(mineral,tax);
        long add = (long)Math.floor(Math.log(tax*2));
        if(add < 0)add = 0;
        return 1l + add;
    }
    
    public UserData randomUser(){
        return UserData.getUD(this.io,Snowflake.of(this.IDList.getData().get(oRan.nextInt(this.IDList.getData().size()))));
    }
}
