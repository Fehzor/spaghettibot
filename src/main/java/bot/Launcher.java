/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bot;

import bot.main.IO;
import data.Field;
import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;
import model.Commons;
import model.Gate;
import threads.PickupEvent;

/**
 *
 * @author FF6EB
 */
public class Launcher {
    public static DiscordClient client;
    
    public static void main(String [] args){
        if(args.length != 1){
            return;
        }
        
        String argument = args[0];
        
        IO io = new IO(argument);
        
        Commons theCommons = new Commons(io);
        
        
        Gate A = new Gate(io, 518831558811975700l);
        
        Gate B = new Gate(io, 621963297142341643l);
        Gate C = new Gate(io, 623042945955921941l);
        
        
        //Gate testServer = new Gate(io, 625559812968677387l);
        
        //jobs = new JobEvent(io);
        //PickupEvent pickup = new PickupEvent(io);
        
        //User fehzor = io.client.getUserById(Snowflake.of(144857966816788482l)).block();
        //pickup.addUser(fehzor);
    }
}
