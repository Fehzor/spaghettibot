/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.command;

import data.UserData;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;
import java.util.List;
import model.Commons;
import model.chatweapons.ChatWeapon;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author FF6EB
 */
public class AuctionCom extends Command{
    public AuctionCom(){
        hidden = false;
        ident = new String[]{
            ">ah",
            ">auction",
            ">auctions"
        };
        description = "Displays auction house!";
        super.addCommand();
    }
    public String execute(Message mess){
        return "**Auctions**\n\n"+Commons.auction;
    }
}
