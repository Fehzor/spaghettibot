/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.command;

import data.UserData;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;
import java.util.List;
import model.chatweapons.ChatWeapon;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author FF6EB
 */
public class ViewWeaponCom extends Command{
    public ViewWeaponCom(){
        hidden = false;
        ident = new String[]{
            ">view",
            ">line"
        };
        description = "Displays a piece of gear's stats!";
        super.addCommand();
    }
    public String execute(Message mess){
        
        String [] split = mess.getContent().get().split(" ",2);
        if(split.length > 1){
            String name = split[1];
            if(name.length() < 3){
                return "Ok at least type out 3 letters otherwise there's gonna be hella results..";
            }
            name = WordUtils.capitalizeFully(name);
            
            ChatWeapon get = ChatWeapon.getWeapon(name);
            
            if(get != null){
                return get.toString();
            }
            
            ChatWeapon [] weps = ChatWeapon.getAllWeapons();
            
            String ret = "Found-\n";
            for(ChatWeapon CW : weps){
                if(CW.name.toLowerCase().contains(name.toLowerCase())){
                    ret+=CW.name+"\n";
                }
            }
            
            return ret;
        }
        
        return "Gear Not Found";
    }
}
