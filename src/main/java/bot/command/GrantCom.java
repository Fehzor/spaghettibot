/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.command;

import static bot.SuperRandom.oRan;
import bot.main.Settings;
import data.UserData;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import model.chatweapons.ChatWeapon;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author FF6EB
 */
public class GrantCom extends Command{
    
    private static final long PRICE = 250000l;
    
    public static final int DROP_CHANCE_RARE = 250;
    public static final int DROP_CHANCE_COMMON = 75;
    
    public GrantCom(){
        hidden = true;
        ident = new String[]{
            ">grant"
        };
        description = "Dev only!";
        super.addCommand();
    }
    
    public String execute(Message mess){
        UserData UD = UserData.getUD(mess.getAuthor().get());
        
        String get = mess.getContent().get().split(" ", 2)[1];
        
        if(UD.snow.asLong() == Settings.FehzorID){
            get = WordUtils.capitalizeFully(get);
            UD.gear.give(get);
            return "You got "+get+"! Hooray!";
        }
        
        return "You aren't a dev!";
    }
}
