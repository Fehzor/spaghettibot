/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.command;

import bot.main.Settings;
import data.UserData;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.Role;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;

/**
 *
 * @author FF6EB
 */
public class SetMine extends Command{
    public SetMine(){
        hidden = false;
        ident = new String[]{
            ">mine"
        };
        description = "Sets what you Mine!";
        super.addCommand();
    }
    public String execute(Message mess){
        
        String [] split = mess.getContent().get().split(" ",2);
        if(split.length > 1){
            String name = split[1];
            
            UserData UD = UserData.getUD(mess.getAuthor().get());
            UD.depositing.set(split[1]);
            
            
            return "Now mining: "+name+"!";
        } else {
        
            return "Enter the color you wish to mine!";
        
        }
    }
}
