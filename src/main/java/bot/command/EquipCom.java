/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.command;

import data.UserData;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author FF6EB
 */
public class EquipCom extends Command{
    public EquipCom(){
        hidden = false;
        ident = new String[]{
            ">equip"
        };
        description = "Equips gear!";
        super.addCommand();
    }
    public String execute(Message mess){
        
        String [] split = mess.getContent().get().split(" ",3);
        if(split.length > 2){
            String slot = split[1];
            String gear = split[2];
            gear = WordUtils.capitalizeFully(gear);
                    
            UserData UD = UserData.getUD(mess.getAuthor().get());
            if(UD.gear.has(gear)){
                if(slot.toLowerCase().equals("a")){
                    UD.gear.take(gear,1);
                    UD.gear.give(UD.weaponA.toString());
                    UD.weaponA.set(gear);
                    return "Equipped gear!";
                }
                if(slot.toLowerCase().equals("b")){
                    UD.gear.take(gear,1);
                    UD.gear.give(UD.weaponB.toString());
                    UD.weaponB.set(gear);
                    return "Equipped gear!";
                }
                if(slot.toLowerCase().equals("c")){
                    UD.gear.take(gear,1);
                    UD.gear.give(UD.weaponC.toString());
                    UD.weaponC.set(gear);
                    return "Equipped gear!";
                }
                return "Slot not found- Use A, B or C!";
            } else {
                return "You don't have that gear!";
            }
        } else {
            return "Useage: Equip A/B/C Gear Name";
        
        }
    }
}
