/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.command;

import static bot.SuperRandom.oRan;
import data.InventoryField;
import data.MapField;
import data.UserData;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import model.chatweapons.ChatWeapon;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author FF6EB
 */
public class BuyCom extends Command{
    
    private static final long PRICE = 250000l;
    
    
    public BuyCom(){
        hidden = false;
        ident = new String[]{
            ">prizebox"
        };
        description = "Buys a prize box!";
        super.addCommand();
        
        since = new InventoryField("BOX","SINCE");
        success = new InventoryField("BOX","SUCCESS");
        bigWin = new InventoryField("BOX","BIGWIN");
    }
    
    public String execute(Message mess){
        UserData UD = UserData.getUD(mess.getAuthor().get());
        if(UD.crowns.getData() < PRICE){
            return "Prize Boxes cost "+PRICE+" crowns.";
        } else {
            UD.crowns.append(-PRICE);
        }
        
        long sin = since.amountOf(UD.snow.asString());
        long suc = success.amountOf(UD.snow.asString());
        long big = bigWin.amountOf(UD.snow.asString());
        
        
        
        if(bigTrial(sin,big,suc)){
            String wep = rareGear[oRan.nextInt(rareGear.length)];
            UD.gear.give(wep);
            bigWin.give(UD.snow.asString(),1l);
            since.take(UD.snow.asString(),since.amountOf(UD.snow.asString()));
            return "You got... "+wep+"!?!??! Aren't you the lucky one!";
        }
        
        if(sucTrial(sin,big,suc)){
            String wep = commonGear[oRan.nextInt(commonGear.length)];
            UD.gear.give(wep);
            success.give(UD.snow.asString(),1l);
            since.take(UD.snow.asString(),since.amountOf(UD.snow.asString()));
            return "You got... "+wep+"! Hooray!";
        }
        
        String type = types[oRan.nextInt(types.length)];
        String costume = costumes[oRan.nextInt(costumes.length)];
        UD.costumes.give(type+" "+costume);
        since.give(UD.snow.asString(),1l);
        
        return "You got... "+type+" "+costume+"! Hooray!";
    }
    
    private InventoryField since;
    private InventoryField success;
    private InventoryField bigWin;
    
    private static int bigOddsStart = 15;
    private static int smallOddsStart = 5;
    
    private static int bigOddsLarge = 250;
    private static int smallOddsLarge = 75;
    
    private static int smallScale = 11;
    private static int bigScale = 3;
    
    private static boolean bigTrial(long sin, long big, long suc){
        int diff = bigOddsLarge - bigOddsStart;
        double percent = ((double)((big))) / ((double)bigScale);
        int odds = bigOddsStart + (int)Math.ceil(percent*diff);
        
        return oRan.nextInt(odds) < sin;
    }
    
    private static boolean sucTrial(long sin, long big, long suc){
        int diff = smallOddsLarge - smallOddsStart;
        double percent = ((double)(suc)) / ((double)smallScale);
        int odds = smallOddsStart + (int)Math.ceil(percent*diff);
        
        return oRan.nextInt(odds) < sin;
    }
    
    private static String [] types = new String [] {
        "Hallow",
        "Frosty",
        "Amethyst",
        "Citrine",
        "Ruby",
        "Sapphire",
        "Opal",
        "Diamond",
        "Surge",
        "Prismatic",
        "Dangerous",
        "Hunter",
        "Lovely",
        "Tech Pink",
        "Tech Blue",
        "Tech Orange",
        "Tech Green",
        "Polar",
        "Ancient",
        "Celestial",
        "Rage",
        "Hazardous",
        "Glacial",
        "Vile",
        "Verdant",
        "Frenzy",
        "Electric",
        "Dazed",
        "Wicked",
        "Blazing",
        "Slumber",
        "Autumn",
        "Late Harvest",
        "Turqoise",
        "Lucky",
        "Dumb",
        "Grey",
        "Tawny"
    };
    
    private static String [] costumes = new String[]{
        "Maid Headband",
        "Bolted Vee",
        "Culet",
        "Sallet",
        "Brigadine",
        "Canteen",
        "Bomb Bandolier",
        "Side Blade",
        "Whatnot",
        "Doo Dad",
        "Headlamp",
        "Headband",
        "Toupee",
        "Dapper Combo",
        "Glasses",
        "Helm-Mounted Display",
        "Power Scanner",
        "Round Shades",
        "Plume",
        "Ribbon",
        "Scarf",
        "Com Unit",
        "Mecha Wings",
        "Vertical Vents",
        "Crest",
        "Parrying Blade",
        "Wings",
        "Crescent Helm",
        "Fur Cap",
        "Hood",
        "Pith Helm",
        "Plate Helm",
        "Round Helm",
        "Scale Helm",
        "Tailed Helm",
        "Winged Helm",
        "Brigandine",
        "Cloak",
        "Cuirass",
        "Draped Armor",
        "Flak Jacket",
        "Fur Coat",
        "Plate Mail",
        "Raiment",
        "Scale Mail",
        "Dragon Wings",
        "Valkyrie Wings"
    };
    
    private static String [] rareGear = new String[]{
        "Kadalborag",
        "Phantom Striker",
        "Mug Of Misery",
        "Mecha Knight Kit",
        "Ranger Signal Flare"
    };
    
    private static String [] commonGear = new String[]{
        "Caladbolg",
        "Celestial Orbitgun",
        "Overcharged Mixmaster",
        "Cool Owlite Wand",
        "Heavy Owlite Wand",
        "Divine Owlite Wand",
        "Mod Calibrator",
        "Rock Salt"
    };
}
