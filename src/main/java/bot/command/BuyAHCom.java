/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.command;

import static bot.SuperRandom.oRan;
import data.UserData;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import model.Commons;
import model.chatweapons.ChatWeapon;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author FF6EB
 */
public class BuyAHCom extends Command{
    
    private static final long PRICE = 250000l;
    
    public static final int DROP_CHANCE_RARE = 250;
    public static final int DROP_CHANCE_COMMON = 75;
    
    public BuyAHCom(){
        hidden = false;
        ident = new String[]{
            ">buy"
        };
        description = "Buys a prize box!";
        super.addCommand();
    }
    
    public String execute(Message mess){
        UserData UD = UserData.getUD(mess.getAuthor().get());
        
        String one = mess.getContent().get().split(" ",2)[1];
        ChatWeapon test = ChatWeapon.getWeapon(WordUtils.capitalizeFully(one));
        
        if(test == null){
            one = mess.getContent().get().split(" ",3)[1];
            try{
                String two = mess.getContent().get().split(" ",3)[2];
                return Commons.buy(UD, two, Long.parseLong(one));
            } catch (Exception E){
                return Commons.buy(UD, one, 1l);
            }
        } else {
            return Commons.buy(UD, one, 1l);
        }
    }
    
}
