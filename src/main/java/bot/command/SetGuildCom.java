/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.command;

import bot.main.Settings;
import data.UserData;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.Role;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;

/**
 *
 * @author FF6EB
 */
public class SetGuildCom extends Command{
    public SetGuildCom(){
        hidden = false;
        ident = new String[]{
            ">setguild"
        };
        description = "Sets your guild!";
        super.addCommand();
    }
    public String execute(Message mess){
        
        String [] split = mess.getContent().get().split(" ",2);
        if(split.length > 1){
            String name = split[1];
            
            UserData UD = UserData.getUD(mess.getAuthor().get());
            UD.SKGuild.set(name);
            try{
                if(name.toLowerCase().equals("circle of magic")){
                    User U = mess.getAuthor().get();
                    Member M = mess.getGuild().block().getMemberById(U.getId()).block();

                    Snowflake NOTroleID = Snowflake.of((long)Settings.NonMemberID);
                    Snowflake PLUSroleID = Snowflake.of((long)Settings.MemberID);

                    M.removeRole(NOTroleID,"Not a member of Circle of Magic").block();
                    M.addRole(PLUSroleID,"Member of Circle of Magic").block();
                } else if(name.toLowerCase().contains("fuck") 
                        || name.toLowerCase().contains("shit")
                        || name.toLowerCase().contains("bitch")
                        || name.toLowerCase().contains("cunt")
                        || name.toLowerCase().contains("cock")
                        || name.toLowerCase().contains("whore")
                        || name.toLowerCase().contains("slut")
                        || name.toLowerCase().contains("ass")){
                    User U = mess.getAuthor().get();
                    Member M = mess.getGuild().block().getMemberById(U.getId()).block();

                    Snowflake NOTroleID = Snowflake.of((long)Settings.MemberID);
                    Snowflake PLUSroleID = Snowflake.of((long)Settings.NonMemberID);
                    Snowflake PLUSfilthID = Snowflake.of((long)Settings.FilthID);

                    M.removeRole(NOTroleID,"Not not a member of Circle of Magic").block();
                    M.addRole(PLUSroleID,"Not a member of Circle of Magic").block();
                    M.addRole(PLUSfilthID,"Filthy ass motherfucker coming on here messing with commands like shit").block();
                } else {
                    User U = mess.getAuthor().get();
                    Member M = mess.getGuild().block().getMemberById(U.getId()).block();

                    Snowflake NOTroleID = Snowflake.of((long)Settings.MemberID);
                    Snowflake PLUSroleID = Snowflake.of((long)Settings.NonMemberID);

                    M.removeRole(NOTroleID,"Not not a member of Circle of Magic").block();
                    M.addRole(PLUSroleID,"Not a member of Circle of Magic").block();
                }
            } catch (Exception E){
                
            }
            
            return "Guild set to "+name+"!";
        } else {
        
            return "Enter your guild name after the command-eg setguild Circle of Magic";
        
        }
    }
}
