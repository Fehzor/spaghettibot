/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bot.command;

import data.UserData;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import model.chatweapons.ChatWeapon;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author FF6EB
 */
public class CraftCom extends Command{
    public CraftCom(){
        hidden = false;
        ident = new String[]{
            ">craft"
        };
        description = "Crafts gear!";
        super.addCommand();
    }
    
    public String execute(Message mess){
        
        String [] split = mess.getContent().get().split(" ",2);
        if(split.length > 1){
            String gear = split[1];
            gear = WordUtils.capitalizeFully(gear);
                    
            UserData UD = UserData.getUD(mess.getAuthor().get());
            
            ChatWeapon get = ChatWeapon.getWeapon(gear);
            if(get == null){
                return "Gear piece doesn't exist (yet?)";
            }
            
            if(get.mats.length == 0){
                return "You can't craft that!";
            }
            
            if(get.pre!=null && !UD.gear.has(get.pre)){
                return "You require the prerequisite- "+get.pre;
            }
            
            for(String S : get.mats){
                if(!UD.materials.has(S,craftingCost(get.stars))){
                    return "You require additional "+S+"... "
                            +craftingCost(get.stars)+" in total, to be exact.";
                }
            }
            
            UD.gear.take(get.pre);
            for(String S : get.mats){
                UD.materials.take(S,craftingCost(get.stars));
            }
            
            UD.gear.give(gear);
            
            return "CRAFTING SUCCESS!";
        } else {
            UserData UD = UserData.getUD(mess.getAuthor().get());
            
            ChatWeapon[]list = ChatWeapon.getAllWeapons();
            
            String ret = "You can craft...\n\n";
            
            for(ChatWeapon CW : list){
                if(this.canCraft(UD, CW.name)){
                    ret+=CW.name+"\n";
                }
            }
            ret+="\nTo craft type: Craft Gear Name";
            return ret;
        }
    }
    
    public static int craftingCost(int stars){
        switch(stars){
            case 1:
                return 1;
            case 2:
                return 10;
            case 3:
                return 100;
            case 4:
                return 1000;
            case 5:
                return 10000;
        }
        return -1;
    }
    
    public boolean canCraft(UserData UD, String gear){
        ChatWeapon get = ChatWeapon.getWeapon(gear);
        if(get == null){
            return false;
        }

        if(get.mats.length == 0){
            return false;
        }

        if(get.pre!=null && !UD.gear.has(get.pre)){
            return false;
        }

        for(String S : get.mats){
            if(!UD.materials.has(S,craftingCost(get.stars))){
                return false;
            }
        }
        return true;
    }
}
